//
//  RecentViewController.h
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/19/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailsViewController.h"

@interface RecentViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
