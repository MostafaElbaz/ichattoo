//
//  CoreDataViewController.h
//  iChatToo
//
//  Created by iBaz on 9/25/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "ViewController.h"

@interface CoreDataViewController : ViewController
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *lineFields;
- (IBAction)btnSave:(id)sender;

@end
