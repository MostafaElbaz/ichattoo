//
//  NewsViewController.m
//  iChatToo
//
//  Created by iBaz on 9/22/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "PhotoGalleryViewController.h"
#import "NewsCell.h"
#import "NewsHeader.h"
#import "ImgViewController.h"

@interface PhotoGalleryViewController () {
    NSArray *recipeImages;
    BOOL shareEnabled;
    NSMutableArray *selectedRecipes;
}



@end

@implementation PhotoGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Initialize recipe image array
    NSArray *mainDishImages = [NSArray arrayWithObjects:@"egg_benedict.jpg", @"full_breakfast.jpg", @"ham_and_cheese_panini.jpg", @"ham_and_egg_sandwich.jpg", @"hamburger.jpg", @"instant_noodle_with_egg.jpg", @"japanese_noodle_with_pork.jpg", @"mushroom_risotto.jpg", @"noodle_with_bbq_pork.jpg", @"thai_shrimp_cake.jpg", @"vegetable_curry.jpg", nil];
    NSArray *drinkDessertImages = [NSArray arrayWithObjects:@"angry_birds_cake.jpg", @"creme_brelee.jpg", @"green_tea.jpg", @"starbucks_coffee.jpg", @"white_chocolate_donut.jpg", nil];
    recipeImages = [NSArray arrayWithObjects:mainDishImages, drinkDessertImages, nil];
    
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.colView.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(20, 0, 20, 0);
//
    selectedRecipes = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [recipeImages count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [[recipeImages objectAtIndex:section] count];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        NewsHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        NSString *title = [[NSString alloc]initWithFormat:@"Group #%i", indexPath.section + 1];
        headerView.lblHdr.text = title;
        UIImage *headerImage = [UIImage imageNamed:@"header_banner.png"];
        headerView.imgHdr.image = headerImage;
        
        reusableview = headerView;
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        
        reusableview = footerview;
    }
    
    return reusableview;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    NewsCell *cell = (NewsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    recipeImageView.image = [UIImage imageNamed:[recipeImages[indexPath.section] objectAtIndex:indexPath.row]];
    
  //   UIImage *headerImage = [UIImage imageNamed:@"instant_noodle_with_egg.jpg"];
  //  cell.imgNews.image = recipeImageView;
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo-frame-2.png"]];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo-frame-selected.png"]];    return cell;
}
//
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segShowPhoto"]) {
        NSArray *indexPaths = [self.colView indexPathsForSelectedItems];
        ImgViewController *destViewController = segue.destinationViewController;
        NSIndexPath *indexPath = [indexPaths objectAtIndex:0];
        destViewController.imgName = [recipeImages[indexPath.section] objectAtIndex:indexPath.row];
        [self.colView deselectItemAtIndexPath:indexPath animated:NO];
    }
}
//
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (shareEnabled) {
        // Determine the selected items by using the indexPath
        NSString *selectedRecipe = [recipeImages[indexPath.section] objectAtIndex:indexPath.row];
        // Add the selected item into the array
        [selectedRecipes addObject:selectedRecipe];
    }
}
//

- (IBAction)btnShareImage_Click:(id)sender {
  
        if (shareEnabled) {
            
            // Post selected photos to Facebook
            if ([selectedRecipes count] > 0) {
                NSString *selectedImageName = @"The selected imaged uploaded successfully " ;
                for (NSString *recipePhoto in selectedRecipes) {
                    selectedImageName = [[NSString alloc]initWithFormat:@"%@ , %@",selectedImageName,recipePhoto];
                       }
                
                
 
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Shared Images" message:selectedImageName delegate:self cancelButtonTitle:@"Close" otherButtonTitles:Nil, nil];
                [alert show];
                //////////////////////////////////////
            }
            
            // Deselect all selected items
            for(NSIndexPath *indexPath in self.colView.indexPathsForSelectedItems) {
                [self.colView deselectItemAtIndexPath:indexPath animated:NO];
            }
            
            // Remove all items from selectedRecipes array
            [selectedRecipes removeAllObjects];
            
            // Change the sharing mode to NO
            shareEnabled = NO;
            self.colView.allowsMultipleSelection = NO;
            self.btnShareImage.title = @"Share";
            [self.btnShareImage setStyle:UIBarButtonItemStylePlain];
            
        } else {
            
            // Change shareEnabled to YES and change the button text to DONE
            shareEnabled = YES;
            self.colView.allowsMultipleSelection = YES;
            self.btnShareImage.title = @"Upload";
            [self.btnShareImage setStyle:UIBarButtonItemStyleDone];
            
        }
    
}
//
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (shareEnabled) {
        NSString *deSelectedRecipe = [recipeImages[indexPath.section] objectAtIndex:indexPath.row];
        [selectedRecipes removeObject:deSelectedRecipe];
    }
}
//
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if (shareEnabled) {
        return NO;
    } else {
        return YES;
    }
}
@end







