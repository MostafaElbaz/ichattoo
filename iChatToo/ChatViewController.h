//
//  ChatViewController.h
//  iChatToo
//
//  Created by iBaz on 9/21/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "userCell.h"
#import "textCell.h"

@interface ChatViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collViewChat;

@end
