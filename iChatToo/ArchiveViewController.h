//
//  ArchiveViewController.h
//  iChatToo
//
//  Created by iBaz on 9/25/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArchiveViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *ttxtState;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
- (IBAction)btnSave:(id)sender;

@end
