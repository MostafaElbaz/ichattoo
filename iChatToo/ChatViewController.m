//
//  ChatViewController.m
//  iChatToo
//
//  Created by iBaz on 9/21/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "ChatViewController.h"

@interface ChatViewController ()

@end

@implementation ChatViewController {

    NSArray *arrOfUsers;
    NSArray *arrOfChatText;
    NSArray *arrHeader;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[self collViewChat]setDataSource:self];
    [[self collViewChat]setDelegate:self];
    
    arrOfUsers = [[NSArray alloc]initWithObjects:@"Mostafa",@"Sa3ied",@"Aya",@"Noha", nil];
    arrOfChatText = [[NSArray alloc]initWithObjects:@"Hi",@"Hello",@"how are you?",@"We are fine" ,nil];
    
    arrHeader = [[NSArray alloc]initWithObjects:@"1",@"2",@"3",@"4" ,nil];
    
        //
  }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    return 1;//[arrOfUsers count];
}
//
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [arrOfUsers count];
}
//
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
   // if(indexPath.section == 0)
  //  {
    NSString *cellUserID = @"cellUser";
   
    //
    
    userCell *usrCell = [self.collViewChat dequeueReusableCellWithReuseIdentifier:cellUserID forIndexPath:indexPath];
    //
    [[usrCell lblUsername]setText:[arrOfUsers objectAtIndex:indexPath.section]];
    [[usrCell lblUserChat]setText:[arrOfChatText objectAtIndex:indexPath.section]];
        return usrCell;
   /* }
   else
    {
         NSString *celltextID = @"cellText";
        textCell *txtCell = [self.collViewChat dequeueReusableCellWithReuseIdentifier:celltextID forIndexPath:indexPath];
        [[txtCell lblChatText]setText:[arrOfChatText objectAtIndex:indexPath.item]];
        return txtCell;
    }
    */
  
}
//


@end






