//
//  ImgViewController.h
//  iChatToo
//
//  Created by iBaz on 9/22/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImgViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblImgName;
- (IBAction)btnClose:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak,nonatomic) NSString *imgName;
@end
