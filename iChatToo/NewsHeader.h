//
//  NewsHeader.h
//  iChatToo
//
//  Created by iBaz on 9/22/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblHdr;
@property (weak, nonatomic) IBOutlet UIImageView *imgHdr;

@end
