//
//  NewsCell.h
//  iChatToo
//
//  Created by iBaz on 9/22/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgNews;

@end
