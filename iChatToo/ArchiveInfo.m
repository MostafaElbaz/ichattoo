//
//  ArchiveInfo.m
//  iChatToo
//
//  Created by iBaz on 9/25/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "ArchiveInfo.h"
static NSString * const kLinesKey = @"kLinesKey";

@implementation ArchiveInfo


#pragma mark - Coding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        NSArray *array =[aDecoder decodeObjectForKey:kLinesKey];
        self.Phone = array[0];
        self.Address = array[1];
        self.State = array[2];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder;
{
    NSArray *array = [[NSArray alloc]initWithObjects:self.Phone,self.Address,self.State, nil];
    
    [aCoder encodeObject:array forKey:kLinesKey];
}

#pragma mark - Copying

- (id)copyWithZone:(NSZone *)zone;
{
    ArchiveInfo *copy = [[[self class] allocWithZone:zone] init];
    NSMutableArray *linesCopy = [NSMutableArray array];
    
        [linesCopy addObject:[self.Phone copyWithZone:zone]];
     [linesCopy addObject:[self.Address copyWithZone:zone]];
     [linesCopy addObject:[self.State copyWithZone:zone]];
    
    copy.Phone = linesCopy[0];
    copy.Address = linesCopy[1];
    copy.State = linesCopy[2];
    return copy;
}


@end
