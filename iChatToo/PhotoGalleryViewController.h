//
//  NewsViewController.h
//  iChatToo
//
//  Created by iBaz on 9/22/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoGalleryViewController : UIViewController
- (IBAction)btnShareImage_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnShareImage;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;

@end
