//
//  ImgViewController.m
//  iChatToo
//
//  Created by iBaz on 9/22/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "ImgViewController.h"

@interface ImgViewController ()

@end

@implementation ImgViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	//
    self.imgView.image = [UIImage imageNamed:self.imgName];
    self.lblImgName.text = self.imgName;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnClose:(id)sender {
     [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
