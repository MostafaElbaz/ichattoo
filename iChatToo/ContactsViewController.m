//
//  ContactsViewController.m
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/19/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "ContactsViewController.h"

@interface ContactsViewController ()

@end

@implementation ContactsViewController {
    NSDictionary *dicContactsWork ;
    NSArray *arrWrkNames;
    NSArray *arrWrkNum;
    //
    NSDictionary *dicContactsFamily;
    NSArray *arrFmlNames;
    NSArray *arrFmlNum;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //Load the contacts from the plist to Dictionary
    NSURL *url = [[NSBundle mainBundle]URLForResource:@"ContactsWork" withExtension:@"plist"];
    dicContactsWork = [NSDictionary dictionaryWithContentsOfURL:url];
    arrWrkNames = dicContactsWork.allKeys;
    arrWrkNum = dicContactsWork.allValues;
    //
    NSURL *urlFml = [[NSBundle mainBundle]URLForResource:@"ContactsFamily" withExtension:@"plist"];
    dicContactsFamily = [NSDictionary dictionaryWithContentsOfURL:urlFml];
    arrFmlNames = dicContactsFamily.allKeys;
    arrFmlNum = dicContactsFamily.allValues;
    //
   }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
     UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    //set the Icon
    UIImage *img = [UIImage imageNamed:@"imgSetContact"];
    [cell.imageView setImage:img];
    //
    if(indexPath.section == 0 )
    {
       cell.textLabel.text = arrWrkNames[indexPath.row];
        cell.detailTextLabel.text = arrWrkNum[indexPath.row];
    }
    else
    {
        cell.textLabel.text = arrFmlNames[indexPath.row];
        cell.detailTextLabel.text = arrFmlNum[indexPath.row];
    }
    return  cell;
}
//
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) {
   return  dicContactsWork.count;
    }
    else
    {
        return dicContactsFamily.count;
    }
}
//
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
//
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
 if(section == 0)
 {
     return @"Work";
 }
    else
    {
        return @"Family";
    }
}
//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *contactName;
    if(indexPath.section == 0)
    {
   contactName = arrWrkNames[indexPath.row];
    
    }
    else
    {
        contactName = arrFmlNames[indexPath.row];
    }
    
    NSString *msg = [[NSString alloc]initWithFormat:@"The contact named %@ %@",contactName,@"has been invited"];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Invited succeeded" message:msg delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
    [alert show];

}
@end






