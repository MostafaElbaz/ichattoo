//
//  SqlLiteViewController.h
//  iChatToo
//
//  Created by iBaz on 9/25/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "ViewController.h"

@interface SqlLiteViewController : ViewController
- (IBAction)btnSave:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFriends;
@property (weak, nonatomic) IBOutlet UITextField *txtCloseFriends;
@property (weak, nonatomic) IBOutlet UITextField *txtFamily;

@end
