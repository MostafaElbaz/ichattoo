//
//  FavViewController.m
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/18/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "FavViewController.h"

@interface FavViewController ()

@end

@implementation FavViewController {
    NSDictionary *dicFav;
    NSArray *arrFavTitle;
    NSArray *arrFavDesc;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //Load the contacts from the plist to Dictionary
    NSURL *url = [[NSBundle mainBundle]URLForResource:@"FavoriteNews" withExtension:@"plist"];
    dicFav = [NSDictionary dictionaryWithContentsOfURL:url];
    arrFavTitle = dicFav.allKeys;
    arrFavDesc = dicFav.allValues;
    //
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    //set the Icon
    UIImage *img = [UIImage imageNamed:@"imgSetFav"];
    [cell.imageView setImage:img];
    
    cell.textLabel.text = arrFavTitle[indexPath.row];
    cell.detailTextLabel.text = arrFavDesc[indexPath.row];
    
    return  cell;
}
//
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  dicFav.count;
    
}
//
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return @"The Recent news";
    
}
//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Move to Details
   // [self performSegueWithIdentifier:@"segFavDetails" sender:self];
/*
NSString *NewsDesc = arrFavDesc[indexPath.row];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"selected news" message:NewsDesc delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
    [alert show];
    */
}
@end
