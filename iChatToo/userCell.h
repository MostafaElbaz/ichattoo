//
//  userCell.h
//  iChatToo
//
//  Created by iBaz on 9/21/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface userCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUserChat;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;

@end
