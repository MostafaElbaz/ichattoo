//
//  FavDetailsViewController.h
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/19/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavDetailsViewController : UIViewController
- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property NSInteger SelectedIndex;

@end
