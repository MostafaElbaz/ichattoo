//
//  ViewController.m
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/18/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnLogin:(id)sender {
    //release the keyboard
    [self.txtUserName resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    //Check the user credential
    NSString *userName = _txtUserName.text;
    NSString *password = _txtPassword.text;
    userName = [userName lowercaseString];
    // Validation
     if ([userName isEqualToString:@""] && [password isEqualToString:@""]) {
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Empty Fields" message:@"The user name or the password are Empty,Please fill them and try again" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
         [alert show];
         return;
     }
    
    if ([userName isEqualToString:@"mostafa"] && [password isEqualToString:@"123"]) {
        //
        //Move to Inbox
        [self performSegueWithIdentifier:@"segLogin" sender:self];
    }
    else {
    //
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Wrong credential" message:@"The user name or the password are incorrect" delegate:nil cancelButtonTitle:@"Try again" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
//

//
-(BOOL)textFieldShouldReturn:(UITextField *)TextField
{
    [TextField resignFirstResponder];
    return YES;
}
//
@end
