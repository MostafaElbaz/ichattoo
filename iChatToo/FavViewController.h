//
//  FavViewController.h
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/18/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
