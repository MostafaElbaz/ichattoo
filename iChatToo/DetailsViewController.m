//
//  DetailsViewController.m
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/19/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
     NSDictionary *dicRecent ;
     NSArray *arrRecTitle;
     NSArray *arrRecDesc;
     
     //Load the contacts from the plist to Dictionary
     NSURL *url = [[NSBundle mainBundle]URLForResource:@"RecentNews" withExtension:@"plist"];
     dicRecent = [NSDictionary dictionaryWithContentsOfURL:url];
     arrRecTitle = dicRecent.allValues;
     arrRecDesc = dicRecent.allValues;
    
    NSInteger index = self.SelectedNewsIndex;
    self.txtTitle.text = arrRecTitle[index];
     self.detailsView.text = arrRecDesc[index];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBack:(id)sender {
    
    //Move to Details
    [self performSegueWithIdentifier:@"segBack" sender:self];
}

@end
