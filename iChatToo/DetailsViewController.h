//
//  DetailsViewController.h
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/19/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *txtTitle;
@property (weak, nonatomic) IBOutlet UITextView *detailsView;
@property (assign, nonatomic) NSInteger SelectedNewsIndex;

- (IBAction)btnBack:(id)sender;



@end
