//
//  ContactsViewController.h
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/19/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
