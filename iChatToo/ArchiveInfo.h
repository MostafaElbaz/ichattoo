//
//  ArchiveInfo.h
//  iChatToo
//
//  Created by iBaz on 9/25/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArchiveInfo : NSObject<NSCopying,NSCoding>
@property (weak,nonatomic) NSString *Phone;
@property (weak,nonatomic) NSString *Address;
@property (weak,nonatomic) NSString *State;

@end
