//
//  RecentViewController.m
//  iChatToo
//
//  Created by Mostafa Elbaz on 9/19/14.
//  Copyright (c) 2014 Mostafa Elbaz. All rights reserved.
//

#import "RecentViewController.h"

@interface RecentViewController ()

@end

@implementation RecentViewController {
    NSDictionary *dicRecent ;
    NSArray *arrRecTitle;
    NSArray *arrRecDesc;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    //Load the contacts from the plist to Dictionary
    NSURL *url = [[NSBundle mainBundle]URLForResource:@"RecentNews" withExtension:@"plist"];
    dicRecent = [NSDictionary dictionaryWithContentsOfURL:url];
    arrRecTitle = dicRecent.allKeys;
    arrRecDesc = dicRecent.allValues;
    //
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    //set the Icon
    UIImage *img = [UIImage imageNamed:@"ImgSetNews"];
    [cell.imageView setImage:img];
 
        cell.textLabel.text = arrRecTitle[indexPath.row];
        cell.detailTextLabel.text = arrRecDesc[indexPath.row];
    
       return  cell;
}
//
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
          return  dicRecent.count;
   
}
//
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
   
        return @"The Recent news";
   
}
//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Move to Details
   // [self performSegueWithIdentifier:@"segDetails" sender:self];
    
    /*
    NSString *NewsDesc = arrRecDesc[indexPath.row];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"selected news" message:NewsDesc delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
    [alert show];
    */
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
  DetailsViewController *dc = [segue destinationViewController];
    NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    dc.SelectedNewsIndex = path.row;
}

@end




